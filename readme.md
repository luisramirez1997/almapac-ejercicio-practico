

## EJERCICIO PRACTICO DE UNA API CON JAVA + SPRING BOOT + H2 + JWT


-Este es un ejercicio creado con el lenguaje de programacion JAVA y el framework spring boot. Se ha desarrollado los diferentes end points
para el registro de usuarios, login, consulta de end points privados haciendo uso de tokens JWT y algunas validaciones extras.

-Validaciones tomadas en cuenta:
    
    -Expresiones regulares para el registro de correo.
    -Numero telefonico requerido.
    -Citycode requerido.
    -Countrycode requerido.


## Base de datos utilizadas H2 en memoria.
-Credenciales para su testeo via navegador `http://localhost:8080/h2-console`

    -JDBC URL: jdbc:h2:mem:db
    -User Name: sa
    -Password: 



## PRE REQUISITOS
- Clonar el proyecto
- Tener JAVA 17 instalado en el equipo.


## Guia de usuario

- 1. git clone https://gitlab.com/luisramirez1997/almapac-ejercicio-practico.git 

- 2. Correr en la terminal y raiz del proyecto ya clonado

```shell
./mvnw spring-boot:run
```

- 3. Abrir la herramienta ya instalada en el proyectto swagger para su testeo


`http://localhost:8080/swagger-ui/index.html`


- 4. registrar un usuario asi como se ejemplifica a continuacion
```json
    {
    "name": "luis",
    "email": "luis@gmail.com",
    "password": "123456",
    "phones": [
        {
        "number": "76107412",
        "citycode": "ss",
        "contrycode": "sv"
        }
    ]
    }

```


- 5. Testar el resto de end points.


## Lista de enpoints publicos sin autentificacion JWT

- 1. End point para registro de usuarios

```shell
http://localhost:8080/api/auth/register
```

- 2. End point para login

```shell
http://localhost:8080/api/auth/login
```

- 3. End point sin verificacion de prueba

```shell
http://localhost:8080/api/public
```


## Lista de enpoints privados con autentificacion JWT

- 1. End point ver listado de usuarios registrados

```shell
http://localhost:8080/api/private/users
```

- 2. End point de prueba privado

```shell
http://localhost:8080/api/private
```

## Video Tutorial

https://drive.google.com/file/d/1RvkU6Kj0wTxYF3KE2x5l3J5zO5SIgavI/view?usp=sharing

## Developed by

    - Luis Ernesto Hernandez Ramirez.
    - Phone: + 503 7601 1036
    - Email: hrlernesto@gmail.com
