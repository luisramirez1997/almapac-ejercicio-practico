package com.auth.userRoutes.dto.response;

import com.auth.models.Phone;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class PhoneResponse {
    private String number;
    private String citycode;
    private String contrycode;
    private String id;

    public static PhoneResponse phoneMapper(Phone phone) {
        return PhoneResponse.builder()
                .id(phone.getId().toString())
                .number(phone.getNumber())
                .citycode(phone.getCitycode())
                .contrycode(phone.getContrycode())
                .build();
    }
}
