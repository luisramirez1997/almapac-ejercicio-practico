package com.auth.userRoutes;

import java.util.List;

import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.EntityResponse;

import com.auth.models.User;
import com.auth.repositores.UserRepository;
import com.auth.userRoutes.dto.response.UserResponse;

import lombok.RequiredArgsConstructor;

@RestController

@RequestMapping("/api/users")
@RequiredArgsConstructor


public class UserController {
    
    private final UserRepository userrepository;

    @GetMapping
    public ResponseEntity<List<UserResponse>> GetAllUsers(){

        var users = this.userrepository.findAll();
        return ResponseEntity.ok(users.stream().map( user -> UserResponse.userMapper(user) ).toList());
        
      }


      

    
}
