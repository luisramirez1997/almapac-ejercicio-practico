package com.auth.repositores;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import com.auth.models.Phone;

public interface PhoneRepository extends JpaRepository<Phone, UUID> {

}
