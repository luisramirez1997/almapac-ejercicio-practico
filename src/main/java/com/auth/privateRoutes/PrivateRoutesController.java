package com.auth.privateRoutes;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth.repositores.UserRepository;
import com.auth.userRoutes.dto.response.UserResponse;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/private")
public class PrivateRoutesController {

    @GetMapping
    public String hello() {
        return "Hello from private route";
    }


     private final UserRepository userrepository;

    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>> GetAllUsers(){

        var users = this.userrepository.findAll();
        return ResponseEntity.ok(users.stream().map( user -> UserResponse.userMapper(user) ).toList());
        
      }


}
