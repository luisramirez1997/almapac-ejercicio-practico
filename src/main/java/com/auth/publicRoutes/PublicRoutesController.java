package com.auth.publicRoutes;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public")
public class PublicRoutesController {

    @GetMapping
    public String hello() {
        return "Hello from public route";
    }

}
