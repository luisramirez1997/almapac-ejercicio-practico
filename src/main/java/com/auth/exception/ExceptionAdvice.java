package com.auth.exception;

import java.nio.file.AccessDeniedException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;

@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(Exception e) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiError apiError = new ApiError(
                "Ha ocurrido un error inesperado");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleValidationErrors(MethodArgumentNotValidException ex) {

        HttpStatus status = HttpStatus.BAD_REQUEST;

        ApiError apiError = new ApiError(
                ex.getBindingResult().getFieldErrors().get(0).getDefaultMessage());

        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ApiError> handleNotFoundException(NotFoundException ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ApiError apiError = new ApiError(ex.getMessage());
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<ApiError> handleConflictException(ConflictException ex) {
        HttpStatus status = HttpStatus.CONFLICT;
        ApiError apiError = new ApiError(ex.getMessage());
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ApiError> handleUsernameNotFoundException(UsernameNotFoundException ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ApiError apiError = new ApiError(
                "No se ha encontrado el usuario solicitado");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(NoResourceFoundException.class)
    public ResponseEntity<ApiError> handleNoResourceFoundException(NoResourceFoundException ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ApiError apiError = new ApiError(
                "No se ha encontrado el recurso solicitado");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiError> handleAccessDeniedException(
            org.springframework.security.access.AccessDeniedException ex) {
        HttpStatus status = HttpStatus.FORBIDDEN;
        ApiError apiError = new ApiError(
                "No tienes permisos para acceder a este recurso");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    // BadCredentialsException

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ApiError> handleBadCredentialsException(
            BadCredentialsException ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        ApiError apiError = new ApiError(
                "Credenciales incorrectas");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<ApiError> handleExpiredJwtException(
            ExpiredJwtException ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        ApiError apiError = new ApiError(
                "El token ha expirado");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<ApiError> handleInvalidJwtAuthenticationException(
            SignatureException ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        ApiError apiError = new ApiError(
                "Token inválido");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

    @ExceptionHandler(MalformedJwtException.class)
    public ResponseEntity<ApiError> handleMalformedJwtException(
            MalformedJwtException ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        ApiError apiError = new ApiError(
                "Token inválido");
        return ResponseEntity
                .status(status)
                .body(apiError);
    }

}
