package com.auth.authRoutes;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth.authRoutes.dto.request.LoginRequest;
import com.auth.authRoutes.dto.request.UserRequest;
import com.auth.authRoutes.dto.response.LoginResponse;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> auth(
            @Valid @RequestBody LoginRequest request) {
        var user = authService.login(request);
        return ResponseEntity.ok(user);
    }

    record AuthResponse(String message) {
    }

    @PostMapping("/register")
    public ResponseEntity<LoginResponse> createUser(@Valid @RequestBody UserRequest request) {
        var user = authService.createUser(request);
        return ResponseEntity.ok(user);
    }

}
