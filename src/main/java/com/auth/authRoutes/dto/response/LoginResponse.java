package com.auth.authRoutes.dto.response;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.auth.models.User;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class LoginResponse {
    private String id;
    private Date created;
    private Date modified;
    private Date lastLogin;
    private String token;
    private boolean isActive;
    private List<PhoneResponse> phones;

    public static LoginResponse userMapper(User user, String token) {
        return LoginResponse.builder()
                .id(user.getId().toString())
                .created(user.getCreated())
                .modified(user.getModified())
                .lastLogin(user.getLastLogin())
                .isActive(user.isActive())
                .token(token)
                .phones(user.getPhones().stream().map(PhoneResponse::phoneMapper).toList())
                .build();
    }
}
