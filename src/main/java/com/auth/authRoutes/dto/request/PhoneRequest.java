package com.auth.authRoutes.dto.request;

import com.auth.models.Phone;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class PhoneRequest {
    @NotEmpty(message = "number is required")
    private String number;
    @NotEmpty(message = "citycode is required")
    private String citycode;
    @NotEmpty(message = "contrycode is required")
    private String contrycode;

    public Phone toPhone() {
        return new Phone(number, citycode, contrycode);
    }
}