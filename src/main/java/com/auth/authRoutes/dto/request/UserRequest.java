package com.auth.authRoutes.dto.request;

import java.util.ArrayList;

import com.auth.models.User;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class UserRequest {

    private static final String EMAIL_REGEX = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";



    @NotBlank(message = "name is required")
    private String name;
    @NotBlank(message = "email is required")
    @Pattern(regexp = EMAIL_REGEX, message = "EMAIL FORMAT WRONG")
    private String email;
    @NotBlank(message = "password is required")
    private String password;

    @NotNull(message = "phones is required")
    @Valid
    private ArrayList<PhoneRequest> phones;

    public User toModel() {

        User user = new User(this.name, this.email, this.password,
                this.phones.stream().map(value -> value.toPhone()).toList());

        return user;
    }
}
