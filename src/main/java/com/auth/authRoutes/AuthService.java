package com.auth.authRoutes;

import com.auth.authRoutes.dto.request.LoginRequest;
import com.auth.authRoutes.dto.request.UserRequest;
import com.auth.authRoutes.dto.response.LoginResponse;

public interface AuthService {
    LoginResponse createUser(UserRequest request);

    LoginResponse login(LoginRequest request);
}
