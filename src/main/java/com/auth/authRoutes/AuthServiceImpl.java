package com.auth.authRoutes;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.auth.authRoutes.dto.request.LoginRequest;
import com.auth.authRoutes.dto.request.UserRequest;
import com.auth.authRoutes.dto.response.LoginResponse;
import com.auth.config.JwtService;
import com.auth.exception.ConflictException;
import com.auth.models.Phone;
import com.auth.repositores.UserRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Transactional
@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    @Transactional(rollbackOn = Exception.class)
    public LoginResponse createUser(UserRequest request) {
        var foundUser = userRepository.findByEmail(request.getEmail());

        if (foundUser.isPresent()) {
            throw new ConflictException("El correo ya está en uso");
        }

        var user = request.toModel();

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (user.getPhones() != null) {
            for (Phone phone : user.getPhones()) {
                phone.setUser(user);
            }
        }

        var savedUser = userRepository.save(user);

        var token = jwtService.generateToken(savedUser);

        return LoginResponse.userMapper(savedUser, token);

    }

    public LoginResponse login(LoginRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new ConflictException("User not found"));

        if (!user.isActive())
            throw new ConflictException("User is not active");

        var token = jwtService.generateToken(user);

        return LoginResponse.userMapper(user, token);

    }
}
